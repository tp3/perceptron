<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE TS>
<TS version="2.1" language="it_IT">
<context>
    <name>BulletRow</name>
    <message>
        <location filename="../qml/BulletRow.qml" line="60"/>
        <source>Correct and incorrect predictions</source>
        <translation>Previsioni corrette e errate</translation>
    </message>
    <message>
        <location filename="../qml/BulletRow.qml" line="67"/>
        <source>Input data</source>
        <translation>Dati in ingresso</translation>
    </message>
</context>
<context>
    <name>Footer</name>
    <message>
        <location filename="../qml/Footer.qml" line="56"/>
        <location filename="../qml/Footer.qml" line="116"/>
        <source>Input button</source>
        <translation>Pulsante di input</translation>
    </message>
    <message>
        <location filename="../qml/Footer.qml" line="93"/>
        <source>Clear statistics</source>
        <translation>Ripristina statistiche</translation>
    </message>
</context>
<context>
    <name>Header</name>
    <message>
        <location filename="../qml/Header.qml" line="29"/>
        <source>Exit</source>
        <translation>Smettere</translation>
    </message>
    <message>
        <location filename="../qml/Header.qml" line="40"/>
        <source>About this application</source>
        <translation>Informazioni su questa app</translation>
    </message>
    <message>
        <location filename="../qml/Header.qml" line="52"/>
        <source>Perceptron</source>
        <translation>Percettrone</translation>
    </message>
</context>
<context>
    <name>Info</name>
    <message>
        <location filename="../qml/Info.qml" line="73"/>
        <source>University of Würzburg, Germany</source>
        <translation>Università di Würzburg, Germania</translation>
    </message>
    <message>
        <location filename="../qml/Info.qml" line="89"/>
        <source>License:</source>
        <translation>Licenza:</translation>
    </message>
    <message>
        <location filename="../qml/Info.qml" line="91"/>
        <source>based on</source>
        <translation>basato su</translation>
    </message>
    <message>
        <location filename="../qml/Info.qml" line="93"/>
        <source>Source code repository:</source>
        <translation>Repository codice sorgente:</translation>
    </message>
    <message>
        <location filename="../qml/Info.qml" line="120"/>
        <source>Language:</source>
        <translation>Linguaggio:</translation>
    </message>
    <message>
        <location filename="../qml/Info.qml" line="151"/>
        <source>The perceptron is the basic element of a neural network.</source>
        <translation>Il perceptron è l&apos;elemento base di una rete neurale.</translation>
    </message>
    <message>
        <location filename="../qml/Info.qml" line="152"/>
        <source>This application shows how the perceptron learns and predicts a binary sequence.</source>
        <translation>Questa applicazione dimostra come il perceptron impara e predice una sequenza binaria.</translation>
    </message>
    <message>
        <location filename="../qml/Info.qml" line="153"/>
        <source>First, press the red and the green button in a repeating (e.g. alternating) pattern:</source>
        <translation>Per prima cosa, premi il pulsante rosso e quello verde in uno schema ripetuto (ad esempio alternato):</translation>
    </message>
    <message>
        <location filename="../qml/Info.qml" line="172"/>
        <source>As you can see, the synaptic weights quickly adjust themselves, and the perceptron predicts the sequence correctly after short time.</source>
        <translation>Come puoi vedere, i pesi sinaptici si adattano rapidamente e il percettore e il percettore predicono la sequenza correttamente dopo breve tempo.</translation>
    </message>
    <message>
        <location filename="../qml/Info.qml" line="173"/>
        <source>Next, press the buttons randomly.</source>
        <translation>Quindi, premere i pulsanti a caso.</translation>
    </message>
    <message>
        <location filename="../qml/Info.qml" line="174"/>
        <source>If your input is perfectly random, only 50% of the predictions are expected to be correct.</source>
        <translation>Se il tuo input è perfettamente casuale, solo il 50% delle previsioni dovrebbe essere corretto.</translation>
    </message>
    <message>
        <location filename="../qml/Info.qml" line="175"/>
        <source>If the gauge shows more than 50%, this indicates hidden correlations, meaning that you are not a perfect random number generator.</source>
        <translation>Se l&apos;indicatore mostra più del 50%, questo indica correlazioni nascoste, il che significa che non sei un generatore di numeri casuali perfetto.</translation>
    </message>
</context>
<context>
    <name>MainView</name>
    <message>
        <location filename="../qml/MainView.qml" line="145"/>
        <source>Prediction of your next input</source>
        <translation>Predizione</translation>
    </message>
    <message>
        <location filename="../qml/MainView.qml" line="185"/>
        <source>Show prediction</source>
        <translation>Visualizza previsione</translation>
    </message>
    <message>
        <location filename="../qml/MainView.qml" line="191"/>
        <source>Check this box to show/hide the prediction</source>
        <translation>Seleziona questa casella per mostrare o nascondere la previsione</translation>
    </message>
</context>
<context>
    <name>MyToggleButton</name>
    <message>
        <location filename="../qml/MyToggleButton.qml" line="67"/>
        <source>Emulate random input</source>
        <translation>Emula input casuali</translation>
    </message>
</context>
<context>
    <name>Percentage</name>
    <message>
        <location filename="../qml/Percentage.qml" line="58"/>
        <source>Average percentage of correctly predicted hits</source>
        <translation>Percentuale media di previsioni corrette</translation>
    </message>
    <message>
        <location filename="../qml/Percentage.qml" line="121"/>
        <source>Correctly predicted hits</source>
        <translation>Sequenze di tasti che sono state previste correttamente</translation>
    </message>
</context>
<context>
    <name>TranslatorSingleton</name>
    <message>
        <location filename="../translator.cpp" line="135"/>
        <source>Automatic</source>
        <translation>Selezione automatica</translation>
    </message>
</context>
<context>
    <name>Weights</name>
    <message>
        <location filename="../qml/Weights.qml" line="55"/>
        <source>Synaptic weights of the perceptron</source>
        <translation>Pesi sinaptici del perceptron</translation>
    </message>
</context>
<context>
    <name>main</name>
    <message>
        <location filename="../qml/main.qml" line="36"/>
        <source>Physics Apps - Perceptron</source>
        <translation>App fisica - Il Perceptron</translation>
    </message>
</context>
</TS>
