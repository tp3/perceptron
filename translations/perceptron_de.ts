<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE TS>
<TS version="2.1" language="de_DE">
<context>
    <name>BulletRow</name>
    <message>
        <location filename="../qml/BulletRow.qml" line="60"/>
        <source>Correct and incorrect predictions</source>
        <translation>Richtige und falsche Vorhersagen</translation>
    </message>
    <message>
        <location filename="../qml/BulletRow.qml" line="67"/>
        <source>Input data</source>
        <translation>Eingegebene Daten</translation>
    </message>
</context>
<context>
    <name>Footer</name>
    <message>
        <location filename="../qml/Footer.qml" line="56"/>
        <location filename="../qml/Footer.qml" line="116"/>
        <source>Input button</source>
        <translation>Eingabe-Schaltfläche</translation>
    </message>
    <message>
        <location filename="../qml/Footer.qml" line="93"/>
        <source>Clear statistics</source>
        <translation>Statistik zurücksetzen</translation>
    </message>
</context>
<context>
    <name>Header</name>
    <message>
        <location filename="../qml/Header.qml" line="29"/>
        <source>Exit</source>
        <translation>Verlassen</translation>
    </message>
    <message>
        <location filename="../qml/Header.qml" line="40"/>
        <source>About this application</source>
        <translation>Über diese Anwendung</translation>
    </message>
    <message>
        <location filename="../qml/Header.qml" line="52"/>
        <source>Perceptron</source>
        <translation>Perzeptron</translation>
    </message>
</context>
<context>
    <name>Info</name>
    <message>
        <location filename="../qml/Info.qml" line="73"/>
        <source>University of Würzburg, Germany</source>
        <translation>Universität Würzburg</translation>
    </message>
    <message>
        <location filename="../qml/Info.qml" line="89"/>
        <source>License:</source>
        <translation>Lizenz:</translation>
    </message>
    <message>
        <location filename="../qml/Info.qml" line="91"/>
        <source>based on</source>
        <translation>basierend auf</translation>
    </message>
    <message>
        <location filename="../qml/Info.qml" line="93"/>
        <source>Source code repository:</source>
        <translation>Quelltext-Repositorium:</translation>
    </message>
    <message>
        <location filename="../qml/Info.qml" line="120"/>
        <source>Language:</source>
        <translation>Sprache:</translation>
    </message>
    <message>
        <location filename="../qml/Info.qml" line="151"/>
        <source>The perceptron is the basic element of a neural network.</source>
        <translation>Das Perzeptron ist das grundlegende Element eines neuronalen Netzwerks.</translation>
    </message>
    <message>
        <location filename="../qml/Info.qml" line="152"/>
        <source>This application shows how the perceptron learns and predicts a binary sequence.</source>
        <translation>Diese Anwendung zeigt, wie ein Perzeptron eine binäre Sequenz lernt und vorhersagt.</translation>
    </message>
    <message>
        <location filename="../qml/Info.qml" line="153"/>
        <source>First, press the red and the green button in a repeating (e.g. alternating) pattern:</source>
        <translation>Drücken Sie zuerst die rote und die grüne Taste in einem sich wiederholenden (z. B. abwechselnden) Muster:</translation>
    </message>
    <message>
        <source>First, press the red and the green button in a repeating (e.g. alternating) pattern:
</source>
        <translation type="obsolete">Drücken Sie zuerst die rote und die grüne Taste in einem sich wiederholenden (z. B. abwechselnden) Muster:</translation>
    </message>
    <message>
        <source>● First, press the red and the green button in a repeating (e.g. alternating) pattern:
</source>
        <translation type="vanished">● Drücken Sie zuerst die rote und die grüne Taste in einem sich wiederholenden Muster (z. B. abwechselnd):</translation>
    </message>
    <message>
        <location filename="../qml/Info.qml" line="172"/>
        <source>As you can see, the synaptic weights quickly adjust themselves, and the perceptron predicts the sequence correctly after short time.</source>
        <translation>Wie Sie sehen, passen sich die synaptischen Gewichte schnell an und das Perzeptron sagt die Sequenz nach kurzer Zeit korrekt voraus.</translation>
    </message>
    <message>
        <location filename="../qml/Info.qml" line="173"/>
        <source>Next, press the buttons randomly.</source>
        <translation>Als nächstes drücken Sie die Tasten zufällig.</translation>
    </message>
    <message>
        <location filename="../qml/Info.qml" line="174"/>
        <source>If your input is perfectly random, only 50% of the predictions are expected to be correct.</source>
        <translation>Wenn Ihre Eingabe vollkommen zufällig ist, ist zu erwarten, dass im Mittel nur 50% der Vorhersagen korrekt sind.</translation>
    </message>
    <message>
        <location filename="../qml/Info.qml" line="175"/>
        <source>If the gauge shows more than 50%, this indicates hidden correlations, meaning that you are not a perfect random number generator.</source>
        <translation>Wenn die Skala mehr als 50% anzeigt, weist dies auf verborgene Korrelationen hin, was bedeutet, dass Sie kein perfekter Zufallszahlengenerator sind.</translation>
    </message>
</context>
<context>
    <name>Introduction</name>
    <message>
        <source>Are you a good random number generator?</source>
        <translation type="vanished">Sind Sie ein guter Zufallszahlengenerator?</translation>
    </message>
    <message>
        <source>Try to press the red and the green button randomly. The perceptron tries to predict your decision.</source>
        <translation type="vanished">Drücken Sie die rote und die grüne Taste so zufällig wie möglich. Das Perzeptron versucht, Ihre Eingabe vorherzusagen.</translation>
    </message>
</context>
<context>
    <name>MainView</name>
    <message>
        <location filename="../qml/MainView.qml" line="145"/>
        <source>Prediction of your next input</source>
        <translation>Vorhersage Ihrer nächsten Eingabe</translation>
    </message>
    <message>
        <location filename="../qml/MainView.qml" line="185"/>
        <source>Show prediction</source>
        <translation>Zeige Vorhersage</translation>
    </message>
    <message>
        <location filename="../qml/MainView.qml" line="191"/>
        <source>Check this box to show/hide the prediction</source>
        <translation>Markieren Sie das Kästchen, um die Vorhersage anzuzeigen</translation>
    </message>
</context>
<context>
    <name>MyToggleButton</name>
    <message>
        <location filename="../qml/MyToggleButton.qml" line="67"/>
        <source>Emulate random input</source>
        <translation>Simulation einer zufälligen Eingabe</translation>
    </message>
</context>
<context>
    <name>Percentage</name>
    <message>
        <location filename="../qml/Percentage.qml" line="58"/>
        <source>Average percentage of correctly predicted hits</source>
        <translation>Mittlerer Prozentsatz der korrekt vorhergesagten Eingaben</translation>
    </message>
    <message>
        <location filename="../qml/Percentage.qml" line="121"/>
        <source>Correctly predicted hits</source>
        <translation>Korrekt vorhergesagte Tastatureingaben</translation>
    </message>
</context>
<context>
    <name>TranslatorSingleton</name>
    <message>
        <location filename="../translator.cpp" line="135"/>
        <source>Automatic</source>
        <translation>Automatische Auswahl</translation>
    </message>
</context>
<context>
    <name>Weights</name>
    <message>
        <location filename="../qml/Weights.qml" line="55"/>
        <source>Synaptic weights of the perceptron</source>
        <translation>Synaptische Gewichtsfaktoren des Perzeptrons</translation>
    </message>
</context>
<context>
    <name>main</name>
    <message>
        <location filename="../qml/main.qml" line="36"/>
        <source>Physics Apps - Perceptron</source>
        <translation>Physik-Apps - Perzeptron</translation>
    </message>
</context>
</TS>
