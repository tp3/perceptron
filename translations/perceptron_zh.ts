<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE TS>
<TS version="2.1" language="zh_CN">
<context>
    <name>BulletRow</name>
    <message>
        <location filename="../qml/BulletRow.qml" line="60"/>
        <source>Correct and incorrect predictions</source>
        <translation>正确和错误的预测</translation>
    </message>
    <message>
        <location filename="../qml/BulletRow.qml" line="67"/>
        <source>Input data</source>
        <translation>输入数据</translation>
    </message>
</context>
<context>
    <name>Footer</name>
    <message>
        <location filename="../qml/Footer.qml" line="56"/>
        <location filename="../qml/Footer.qml" line="116"/>
        <source>Input button</source>
        <translation>输入按钮</translation>
    </message>
    <message>
        <location filename="../qml/Footer.qml" line="93"/>
        <source>Clear statistics</source>
        <translation>重置统计信息</translation>
    </message>
</context>
<context>
    <name>Header</name>
    <message>
        <location filename="../qml/Header.qml" line="29"/>
        <source>Exit</source>
        <translation>放弃</translation>
    </message>
    <message>
        <location filename="../qml/Header.qml" line="40"/>
        <source>About this application</source>
        <translation>关于这个应用程序</translation>
    </message>
    <message>
        <location filename="../qml/Header.qml" line="52"/>
        <source>Perceptron</source>
        <translation>感知器</translation>
    </message>
</context>
<context>
    <name>Info</name>
    <message>
        <location filename="../qml/Info.qml" line="73"/>
        <source>University of Würzburg, Germany</source>
        <translation>德国维尔茨堡大学</translation>
    </message>
    <message>
        <location filename="../qml/Info.qml" line="89"/>
        <source>License:</source>
        <translation>执照：</translation>
    </message>
    <message>
        <location filename="../qml/Info.qml" line="91"/>
        <source>based on</source>
        <translation>基于</translation>
    </message>
    <message>
        <location filename="../qml/Info.qml" line="93"/>
        <source>Source code repository:</source>
        <translation>源代码存储库：</translation>
    </message>
    <message>
        <location filename="../qml/Info.qml" line="120"/>
        <source>Language:</source>
        <translation>语言：</translation>
    </message>
    <message>
        <location filename="../qml/Info.qml" line="151"/>
        <source>The perceptron is the basic element of a neural network.</source>
        <translation>感知器是神经网络的基本元素。</translation>
    </message>
    <message>
        <location filename="../qml/Info.qml" line="152"/>
        <source>This application shows how the perceptron learns and predicts a binary sequence.</source>
        <translation>该应用程序演示了感知器如何学习和预测二进制序列。</translation>
    </message>
    <message>
        <location filename="../qml/Info.qml" line="153"/>
        <source>First, press the red and the green button in a repeating (e.g. alternating) pattern:</source>
        <translation>首先，按重复（例如交替）模式按下红色和绿色按钮：</translation>
    </message>
    <message>
        <location filename="../qml/Info.qml" line="172"/>
        <source>As you can see, the synaptic weights quickly adjust themselves, and the perceptron predicts the sequence correctly after short time.</source>
        <translation>正如您所看到的，突触权重可以快速调整自己，并且感知器会在短时间内正确预测序列。</translation>
    </message>
    <message>
        <location filename="../qml/Info.qml" line="173"/>
        <source>Next, press the buttons randomly.</source>
        <translation>接下来，随机按下按钮。</translation>
    </message>
    <message>
        <location filename="../qml/Info.qml" line="174"/>
        <source>If your input is perfectly random, only 50% of the predictions are expected to be correct.</source>
        <translation>如果您的输入完全是随机的，那么预计只有50％的预测是正确的。</translation>
    </message>
    <message>
        <location filename="../qml/Info.qml" line="175"/>
        <source>If the gauge shows more than 50%, this indicates hidden correlations, meaning that you are not a perfect random number generator.</source>
        <translation>如果仪表显示超过50％，则表示隐藏的相关性。 这意味着您不是一个完美的随机数生成器。</translation>
    </message>
</context>
<context>
    <name>MainView</name>
    <message>
        <location filename="../qml/MainView.qml" line="145"/>
        <source>Prediction of your next input</source>
        <translation>预测</translation>
    </message>
    <message>
        <location filename="../qml/MainView.qml" line="185"/>
        <source>Show prediction</source>
        <translation>显示预测</translation>
    </message>
    <message>
        <location filename="../qml/MainView.qml" line="191"/>
        <source>Check this box to show/hide the prediction</source>
        <translation>选中此框可显示或隐藏预测</translation>
    </message>
</context>
<context>
    <name>MyToggleButton</name>
    <message>
        <location filename="../qml/MyToggleButton.qml" line="67"/>
        <source>Emulate random input</source>
        <translation>模拟随机输入</translation>
    </message>
</context>
<context>
    <name>Percentage</name>
    <message>
        <location filename="../qml/Percentage.qml" line="58"/>
        <source>Average percentage of correctly predicted hits</source>
        <translation>正确预测的平均百分比</translation>
    </message>
    <message>
        <location filename="../qml/Percentage.qml" line="121"/>
        <source>Correctly predicted hits</source>
        <translation>已经正确预测的击键</translation>
    </message>
</context>
<context>
    <name>TranslatorSingleton</name>
    <message>
        <location filename="../translator.cpp" line="135"/>
        <source>Automatic</source>
        <translation>自动选择</translation>
    </message>
</context>
<context>
    <name>Weights</name>
    <message>
        <location filename="../qml/Weights.qml" line="55"/>
        <source>Synaptic weights of the perceptron</source>
        <translation>突触重量</translation>
    </message>
</context>
<context>
    <name>main</name>
    <message>
        <location filename="../qml/main.qml" line="36"/>
        <source>Physics Apps - Perceptron</source>
        <translation>物理应用程序 - 感知器</translation>
    </message>
</context>
</TS>
