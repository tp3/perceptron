<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE TS>
<TS version="2.1" language="ko_KR">
<context>
    <name>BulletRow</name>
    <message>
        <location filename="../qml/BulletRow.qml" line="60"/>
        <source>Correct and incorrect predictions</source>
        <translation>예측의 정확성 및 부정확성</translation>
    </message>
    <message>
        <location filename="../qml/BulletRow.qml" line="67"/>
        <source>Input data</source>
        <translation>입력 데이터</translation>
    </message>
</context>
<context>
    <name>Footer</name>
    <message>
        <location filename="../qml/Footer.qml" line="56"/>
        <location filename="../qml/Footer.qml" line="116"/>
        <source>Input button</source>
        <translation>입력 버튼</translation>
    </message>
    <message>
        <location filename="../qml/Footer.qml" line="93"/>
        <source>Clear statistics</source>
        <translation>통계 재설정</translation>
    </message>
</context>
<context>
    <name>Header</name>
    <message>
        <location filename="../qml/Header.qml" line="29"/>
        <source>Exit</source>
        <translation>떠나다</translation>
    </message>
    <message>
        <location filename="../qml/Header.qml" line="40"/>
        <source>About this application</source>
        <translation>이 앱 정보</translation>
    </message>
    <message>
        <location filename="../qml/Header.qml" line="52"/>
        <source>Perceptron</source>
        <translation>퍼셉트론</translation>
    </message>
</context>
<context>
    <name>Info</name>
    <message>
        <location filename="../qml/Info.qml" line="73"/>
        <source>University of Würzburg, Germany</source>
        <translation>독일 뷔르츠부르크 대학교</translation>
    </message>
    <message>
        <location filename="../qml/Info.qml" line="89"/>
        <source>License:</source>
        <translation>특허:</translation>
    </message>
    <message>
        <location filename="../qml/Info.qml" line="91"/>
        <source>based on</source>
        <translation>~에 근거하여</translation>
    </message>
    <message>
        <location filename="../qml/Info.qml" line="93"/>
        <source>Source code repository:</source>
        <translation>소스 코드 저장소 :</translation>
    </message>
    <message>
        <location filename="../qml/Info.qml" line="120"/>
        <source>Language:</source>
        <translation>언어:</translation>
    </message>
    <message>
        <location filename="../qml/Info.qml" line="151"/>
        <source>The perceptron is the basic element of a neural network.</source>
        <translation>퍼셉트론은 신경망의 기본 요소입니다.</translation>
    </message>
    <message>
        <location filename="../qml/Info.qml" line="152"/>
        <source>This application shows how the perceptron learns and predicts a binary sequence.</source>
        <translation>이 응용 프로그램은 퍼셉트론 이 어떻게 바이너리 시퀀스를 학습하고 예측 하는지를 보여줍니다.</translation>
    </message>
    <message>
        <location filename="../qml/Info.qml" line="153"/>
        <source>First, press the red and the green button in a repeating (e.g. alternating) pattern:</source>
        <translation>먼저 반복되는 (예 : 번갈아 가면서) 패턴에서 빨간색과 녹색 버튼을 누릅니다:</translation>
    </message>
    <message>
        <location filename="../qml/Info.qml" line="172"/>
        <source>As you can see, the synaptic weights quickly adjust themselves, and the perceptron predicts the sequence correctly after short time.</source>
        <translation>보시다시피, 시냅스 가중치는 빠르게 조정되며 퍼셉트론 은 짧은 시간 후에 시퀀스를 올바르게 예측합니다.</translation>
    </message>
    <message>
        <location filename="../qml/Info.qml" line="173"/>
        <source>Next, press the buttons randomly.</source>
        <translation>그런 다음 버튼을 무작위로 누릅니다.</translation>
    </message>
    <message>
        <location filename="../qml/Info.qml" line="174"/>
        <source>If your input is perfectly random, only 50% of the predictions are expected to be correct.</source>
        <translation>입력이 완벽하게 무작위 인 경우 예측의 50 % 만 정확할 것으로 예상됩니다.</translation>
    </message>
    <message>
        <location filename="../qml/Info.qml" line="175"/>
        <source>If the gauge shows more than 50%, this indicates hidden correlations, meaning that you are not a perfect random number generator.</source>
        <translation>게이지가 50 %를 초과하면 숨겨진 상관 관계가 있음을 나타냅니다. 이것은 당신이 완벽한 난수 생성기가 아니라는 것을 의미합니다.</translation>
    </message>
</context>
<context>
    <name>MainView</name>
    <message>
        <location filename="../qml/MainView.qml" line="145"/>
        <source>Prediction of your next input</source>
        <translation>예측</translation>
    </message>
    <message>
        <location filename="../qml/MainView.qml" line="185"/>
        <source>Show prediction</source>
        <translation>디스플레이 예측</translation>
    </message>
    <message>
        <location filename="../qml/MainView.qml" line="191"/>
        <source>Check this box to show/hide the prediction</source>
        <translation>예측을 표시하거나 숨기려면이 상자를 선택하십시오</translation>
    </message>
</context>
<context>
    <name>MyToggleButton</name>
    <message>
        <location filename="../qml/MyToggleButton.qml" line="67"/>
        <source>Emulate random input</source>
        <translation>무작위 입력 에뮬레이션</translation>
    </message>
</context>
<context>
    <name>Percentage</name>
    <message>
        <location filename="../qml/Percentage.qml" line="58"/>
        <source>Average percentage of correctly predicted hits</source>
        <translation>올바르게 예측 된 키 입력의 평균 비율</translation>
    </message>
    <message>
        <location filename="../qml/Percentage.qml" line="121"/>
        <source>Correctly predicted hits</source>
        <translation>올바르게 예측 된 키 입력</translation>
    </message>
</context>
<context>
    <name>TranslatorSingleton</name>
    <message>
        <location filename="../translator.cpp" line="135"/>
        <source>Automatic</source>
        <translation>자동 선택</translation>
    </message>
</context>
<context>
    <name>Weights</name>
    <message>
        <location filename="../qml/Weights.qml" line="55"/>
        <source>Synaptic weights of the perceptron</source>
        <translation>퍼셉트론의 시냅스 가중치</translation>
    </message>
</context>
<context>
    <name>main</name>
    <message>
        <location filename="../qml/main.qml" line="36"/>
        <source>Physics Apps - Perceptron</source>
        <translation>물리학 앱 - 퍼셉트론</translation>
    </message>
</context>
</TS>
