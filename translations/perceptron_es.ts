<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE TS>
<TS version="2.1" language="es_ES">
<context>
    <name>BulletRow</name>
    <message>
        <location filename="../qml/BulletRow.qml" line="60"/>
        <source>Correct and incorrect predictions</source>
        <translation>Predicciones correctas e incorrectas</translation>
    </message>
    <message>
        <location filename="../qml/BulletRow.qml" line="67"/>
        <source>Input data</source>
        <translation>Datos de entrada</translation>
    </message>
</context>
<context>
    <name>Footer</name>
    <message>
        <location filename="../qml/Footer.qml" line="56"/>
        <location filename="../qml/Footer.qml" line="116"/>
        <source>Input button</source>
        <translation>Botón de entrada</translation>
    </message>
    <message>
        <location filename="../qml/Footer.qml" line="93"/>
        <source>Clear statistics</source>
        <translation>Reiniciar las estadísticas</translation>
    </message>
</context>
<context>
    <name>Header</name>
    <message>
        <location filename="../qml/Header.qml" line="29"/>
        <source>Exit</source>
        <translation>Dejar</translation>
    </message>
    <message>
        <location filename="../qml/Header.qml" line="40"/>
        <source>About this application</source>
        <translation>Acerca de esta aplicación</translation>
    </message>
    <message>
        <location filename="../qml/Header.qml" line="52"/>
        <source>Perceptron</source>
        <translation>Perceptrón</translation>
    </message>
</context>
<context>
    <name>Info</name>
    <message>
        <location filename="../qml/Info.qml" line="73"/>
        <source>University of Würzburg, Germany</source>
        <translation>Universidad de Würzburg, Alemania</translation>
    </message>
    <message>
        <location filename="../qml/Info.qml" line="89"/>
        <source>License:</source>
        <translation>Licencia:</translation>
    </message>
    <message>
        <location filename="../qml/Info.qml" line="91"/>
        <source>based on</source>
        <translation>basado en</translation>
    </message>
    <message>
        <location filename="../qml/Info.qml" line="93"/>
        <source>Source code repository:</source>
        <translation>Repositorio de código fuente:</translation>
    </message>
    <message>
        <location filename="../qml/Info.qml" line="120"/>
        <source>Language:</source>
        <translation>Idioma:</translation>
    </message>
    <message>
        <location filename="../qml/Info.qml" line="151"/>
        <source>The perceptron is the basic element of a neural network.</source>
        <translation>El perceptrón es el elemento básico de una red neuronal.</translation>
    </message>
    <message>
        <location filename="../qml/Info.qml" line="152"/>
        <source>This application shows how the perceptron learns and predicts a binary sequence.</source>
        <translation>Esta aplicación muestra cómo el perceptrón aprende y predice una secuencia binaria.</translation>
    </message>
    <message>
        <location filename="../qml/Info.qml" line="153"/>
        <source>First, press the red and the green button in a repeating (e.g. alternating) pattern:</source>
        <translation>Primero, presione los botones rojo y verde en un patrón de repetición (por ejemplo, alternando):</translation>
    </message>
    <message>
        <location filename="../qml/Info.qml" line="172"/>
        <source>As you can see, the synaptic weights quickly adjust themselves, and the perceptron predicts the sequence correctly after short time.</source>
        <translation>Como puede ver, los pesos sinápticos se ajustan rápidamente y el perceptrón predice la secuencia correctamente después de poco tiempo.</translation>
    </message>
    <message>
        <location filename="../qml/Info.qml" line="173"/>
        <source>Next, press the buttons randomly.</source>
        <translation>A continuación, presione los botones al azar.</translation>
    </message>
    <message>
        <location filename="../qml/Info.qml" line="174"/>
        <source>If your input is perfectly random, only 50% of the predictions are expected to be correct.</source>
        <translation>Si su entrada es perfectamente aleatoria, se espera que solo el 50% de las predicciones sean correctas.</translation>
    </message>
    <message>
        <location filename="../qml/Info.qml" line="175"/>
        <source>If the gauge shows more than 50%, this indicates hidden correlations, meaning that you are not a perfect random number generator.</source>
        <translation>Si el indicador muestra más del 50%, esto indica correlaciones ocultas, lo que significa que no es un generador de números aleatorios perfecto.</translation>
    </message>
</context>
<context>
    <name>MainView</name>
    <message>
        <location filename="../qml/MainView.qml" line="145"/>
        <source>Prediction of your next input</source>
        <translation>Predicción</translation>
    </message>
    <message>
        <location filename="../qml/MainView.qml" line="185"/>
        <source>Show prediction</source>
        <translation>Mostrar predicción</translation>
    </message>
    <message>
        <location filename="../qml/MainView.qml" line="191"/>
        <source>Check this box to show/hide the prediction</source>
        <translation>Marque esta casilla para mostrar u ocultar la predicción</translation>
    </message>
</context>
<context>
    <name>MyToggleButton</name>
    <message>
        <location filename="../qml/MyToggleButton.qml" line="67"/>
        <source>Emulate random input</source>
        <translation>Emular entradas aleatorias</translation>
    </message>
</context>
<context>
    <name>Percentage</name>
    <message>
        <location filename="../qml/Percentage.qml" line="58"/>
        <source>Average percentage of correctly predicted hits</source>
        <translation>Porcentaje promedio de predicciones correctas</translation>
    </message>
    <message>
        <location filename="../qml/Percentage.qml" line="121"/>
        <source>Correctly predicted hits</source>
        <translation>Pulsaciones de teclado que se han pronosticado correctamente</translation>
    </message>
</context>
<context>
    <name>TranslatorSingleton</name>
    <message>
        <location filename="../translator.cpp" line="135"/>
        <source>Automatic</source>
        <translation>Selección automática</translation>
    </message>
</context>
<context>
    <name>Weights</name>
    <message>
        <location filename="../qml/Weights.qml" line="55"/>
        <source>Synaptic weights of the perceptron</source>
        <translation>Pesos sinápticos del perceptrón</translation>
    </message>
</context>
<context>
    <name>main</name>
    <message>
        <location filename="../qml/main.qml" line="36"/>
        <source>Physics Apps - Perceptron</source>
        <translation>Aplicaciones de Física - El Perceptrón</translation>
    </message>
</context>
</TS>
