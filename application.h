/*****************************************************************************
 * Copyright 2019 Haye Hinrichsen
 * Based on an earlier code by Wolfgang Kinzel and Georg Reents
 *
 * This file is part of Perceptron.
 *
 * Perceptron is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by the
 * Free Software Foundation, either version 3 of the License, or (at your
 * option) any later version.
 *
 * Perceptron is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
 * or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License 
 * for more details.
 *
 * You should have received a copy of the GNU General Public License along 
 * with Perceptron. If not, see http://www.gnu.org/licenses/.
 *****************************************************************************/

#ifndef APPLICATION_H
#define APPLICATION_H

#include <QObject>
#include <QVariant>
#include <QQmlApplicationEngine>
#include <QVector>

class Application : public QObject
{
    Q_OBJECT
public:
    Application();
    void init(QQmlApplicationEngine &engine);

public slots:
    void getInput (int value);

signals:
    void sendPrediction (QVariant value);
    void sendWeight (QVariant slot, QVariant weight);

private:
    void predict();
    void learn (int input);

private:
    int N;              // number of weights
    QVector<int> S;     // state of neuron
    QVector<double> J;  // weight factor of the perceptron
    double h;           // sum of weighted inputs
};

#endif // APPLICATION_H
