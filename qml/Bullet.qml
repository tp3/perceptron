/*****************************************************************************
 * Copyright 2019 Haye Hinrichsen
 * Based on an earlier code by Wolfgang Kinzel and Georg Reents
 *
 * This file is part of Perceptron.
 *
 * Perceptron is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by the
 * Free Software Foundation, either version 3 of the License, or (at your
 * option) any later version.
 *
 * Perceptron is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
 * or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License 
 * for more details.
 *
 * You should have received a copy of the GNU General Public License along 
 * with Perceptron. If not, see http://www.gnu.org/licenses/.
 *****************************************************************************/

import QtQuick 2.12

Item {
    property int status: 0
    property bool hidden: false
    property color col: status===1 ? "#f00" : status===-1 ? "#0c0" : "#eee"
    property color darkcol: status===1 ? "#500" : status===-1 ? "#050" : "#ccc"
    Rectangle {
        id: rect
        anchors.horizontalCenter: parent.horizontalCenter
        height: parent.height
        width: parent.height
        radius: height/2
        gradient: Gradient {
            GradientStop { position: 0 ; color: "#fff" }
            GradientStop { position: 1 ; color: hidden ? "#222": darkcol }
        }
    }
    Rectangle {
        id: rect2
        anchors.fill: rect
        anchors.margins: rect.width*0.1
        color: hidden ? "gray" : col
        radius: rect.radius*0.9
    }
    Text {
        visible: status !== 0 && ! hidden
        text: status===1 ? "+":"-"
        color: "#222"
        verticalAlignment: Text.AlignVCenter
        horizontalAlignment: Text.AlignHCenter
        anchors.fill: rect2
        anchors.bottomMargin: rect2.height*0.27
        font.family: window.myfont
        font.pixelSize: height*1.4
    }
}
