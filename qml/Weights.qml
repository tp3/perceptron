/*****************************************************************************
 * Copyright 2019 Haye Hinrichsen
 * Based on an earlier code by Wolfgang Kinzel and Georg Reents
 *
 * This file is part of Perceptron.
 *
 * Perceptron is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by the
 * Free Software Foundation, either version 3 of the License, or (at your
 * option) any later version.
 *
 * Perceptron is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
 * or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License 
 * for more details.
 *
 * You should have received a copy of the GNU General Public License along 
 * with Perceptron. If not, see http://www.gnu.org/licenses/.
 *****************************************************************************/

import QtQuick 2.12
import QtQuick.Controls 2.4

Item {
    readonly property color gridColor: "#ccc"
    property int cellwidth: 10

    property var weights: [0.1,0.5,1,-1,0.01,-0.2,-0.5,1,1.2,-1,0,0.1,0.2]

    property int lastSlot: window.lastSlot;
    property double lastWeight: window.lastWeight;
    onLastSlotChanged: {
        if (lastSlot>=0) bars.itemAt(lastSlot).value = Math.max(-1,Math.min(1,lastWeight));
    }


    Rectangle {
        id: field
        width: slots*cellwidth
        height: parent.height
        color: "black"
        opacity: 0.3
    }

    Rectangle {
        width: cellwidth*10
        y: parent.height/2
        height: parent.height*0.01
        color: gridColor
    }

    MyToolTip {
        width: cellwidth*10
        height: parent.height
        text: qsTr("Synaptic weights of the perceptron")
    }

    Repeater {
        model: slots-1
        Rectangle {
            x: cellwidth * (index+1)
            width: 1
            height: parent.height
            color: "#888"
        }
    }

    Rectangle {
        anchors.fill: field
        width: slots*cellwidth
        height: parent.height
        border.width: 1
        border.color: gridColor
        color: "transparent"
    }

    Repeater {
        id: bars
        model: slots
        Item {
            id: column
            property double value: 0
            property double slidingValue: 0
            PropertyAnimation {
                id: animation
                target: column
                property: "slidingValue"
                to: value
                duration: 300
            }
            onValueChanged: animation.restart()

            x: index*cellwidth
            y: parent.height/2*(slidingValue<=0 ? 1 : 1-slidingValue)
            width: cellwidth
            height: parent.height*Math.abs(slidingValue)/2;
            Rectangle {
                id: outerRect
                anchors.fill: parent
                anchors.rightMargin: cellwidth*0.1
                anchors.leftMargin: cellwidth*0.1
                radius: cellwidth*0.1
                color: "#5973f3"
                gradient: Gradient {
                       GradientStop { position: 0.0; color: "lightsteelblue" }
                       GradientStop { position: 1.0; color: "black" }
                   }
            }
            Rectangle {
                anchors.fill: outerRect
                anchors.margins: cellwidth*0.04
                radius: cellwidth*0.1
                color: "#5973f3"
            }
        }
    }
}




