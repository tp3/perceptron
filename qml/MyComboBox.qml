/*****************************************************************************
 * Copyright 2019 Haye Hinrichsen
 * Based on an earlier code by Wolfgang Kinzel and Georg Reents
 *
 * This file is part of Perceptron.
 *
 * Perceptron is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by the
 * Free Software Foundation, either version 3 of the License, or (at your
 * option) any later version.
 *
 * Perceptron is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
 * or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License 
 * for more details.
 *
 * You should have received a copy of the GNU General Public License along 
 * with Perceptron. If not, see http://www.gnu.org/licenses/.
 *****************************************************************************/


import QtQuick 2.11
import QtQuick.Controls 2.2

ComboBox {
    id: control
    property int fontsize: 10
    wheelEnabled:  false
    font.family: myfont
    font.pixelSize: fontsize

    delegate: ItemDelegate {
         width: control.width
         contentItem: Text {
             text: modelData
             color: "black"
             font: control.font
             elide: Text.ElideRight
             verticalAlignment: Text.AlignVCenter
         }
         highlighted: control.highlightedIndex === index
     }
    indicator: Canvas {
        id: canvas
        x: control.width - width - control.rightPadding
        y: control.topPadding + (control.availableHeight - height) / 2
        width: control.height*0.5
        height: control.height*0.35
        contextType: "2d"

        Connections {
            target: control
            onPressedChanged: canvas.requestPaint()
        }

        onPaint: {
            context.reset();
            context.moveTo(0, 0);
            context.lineTo(width, 0);
            context.lineTo(width / 2, height);
            context.closePath();
            context.fillStyle = control.pressed ? "#a06060" : "#606060";
            context.fill();
        }
    }


    background: Rectangle {
         implicitWidth: 120
         implicitHeight: 40
         border.color: control.pressed ? "white" : "#a0a0a0"
         border.width: control.height*0.04
         radius: 2
     }
    contentItem: Text {
         leftPadding: parent.width * 0.02
         rightPadding: control.indicator.width + control.spacing

         text: control.displayText
         font: control.font
         color: "#000000"
         verticalAlignment: Text.AlignVCenter
         elide: Text.ElideRight
     }
    popup: Popup {
          y: control.height - 1
          width: control.width
          implicitHeight: contentItem.implicitHeight
          padding: 1

          contentItem: ListView {
              clip: true
              implicitHeight: contentHeight
              model: control.popup.visible ? control.delegateModel : null
              currentIndex: control.highlightedIndex
              ScrollIndicator.vertical: ScrollIndicator {
              }
          }

          background: Rectangle {
              border.color: "gray"
              radius: 2
          }
      }
}
