/*****************************************************************************
 * Copyright 2019 Haye Hinrichsen
 * Based on an earlier code by Wolfgang Kinzel and Georg Reents
 *
 * This file is part of Perceptron.
 *
 * Perceptron is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by the
 * Free Software Foundation, either version 3 of the License, or (at your
 * option) any later version.
 *
 * Perceptron is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
 * or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License 
 * for more details.
 *
 * You should have received a copy of the GNU General Public License along 
 * with Perceptron. If not, see http://www.gnu.org/licenses/.
 *****************************************************************************/


import QtQuick 2.2
import QtQuick.Controls 1.4
import QtQuick.Controls.Styles 1.4
import QtQuick.Extras 1.4
import QtQuick.Controls 2.4

Rectangle {
    color: "#222"
    property int fontsize: landscape ? parent.height*0.05 : parent.width*0.03

    property var memory: [0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0]
    readonly property int memorySize: memory.length
    property int n: 0
    property int correctGuess: window.correctGuess
    property bool resetStatistics: window.resetStatistics

    onCorrectGuessChanged: {
        if (correctGuess != 0) {
            memory[n] = correctGuess;
            n = (n+1)%memorySize;
            var sum=0; var total=0;
            for (var i=0; i<memorySize; i++) { total+=Math.abs(memory[i]); sum += memory[i]; }
            if (total>0) gauge.value = 50+50*sum/total;
        }
    }

    onResetStatisticsChanged: {
        if (resetStatistics) {
            for (n=0; n<memorySize; n++) memory[n]=0;
            n=0;
            gauge.value=50;
        }
    }

    MyToolTip {
        anchors.fill: parent
        text: qsTr("Average percentage of correctly predicted hits")
    }

    Gauge {
        id: gauge
        anchors.fill: parent
        anchors.topMargin: landscape ? 0 : parent.height*0.06

        orientation: landscape ? Qt.Vertical : Qt.Horizontal
        minimumValue: 0
        maximumValue: 100
        value: 50
        font.pixelSize: fontsize
        font.family: window.myfont
        Behavior on value { NumberAnimation { duration: 500 }}

        style: GaugeStyle {

            minorTickmark: Item {
                implicitWidth: fontsize*0.3
                implicitHeight: 1

                Rectangle {
                    color: "#cccccc"
                    anchors.fill: parent
                    anchors.leftMargin: fontsize*0.1
                    anchors.rightMargin: fontsize*0.1
                }
            }

            tickmark: Item {
                implicitWidth: fontsize*0.6
                implicitHeight: 1

                Rectangle {
                    color: "#c8c8c8"
                    anchors.fill: parent
                    anchors.leftMargin: fontsize*0.1
                    anchors.rightMargin: fontsize*0.1
                }
            }

            valueBar: Rectangle {
                implicitWidth: fontsize
                readonly property double fraction: gauge.value/ gauge.maximumValue
                border.color: "gray"
                color: fraction < 0.5001 ? "red" : Qt.rgba(2-2*fraction, fraction, 0, 1)
                radius: 0
            }
        }
    }
    Text {
        color: "white"
        text: "%"
        anchors.right: parent.right
        anchors.rightMargin: parent.width*0.1
        anchors.verticalCenter: parent.verticalCenter
        font.family: window.myfont
        font.pixelSize: fontsize
        visible: landscape
    }

    Text {
        text: qsTr("Correctly predicted hits") +  " [%]"
        horizontalAlignment: Text.AlignHCenter
        color: "gray"
        font.family: window.myfont
        anchors.horizontalCenter: parent.horizontalCenter
        anchors.bottom: parent.bottom
        anchors.bottomMargin: parent.height*0.09
        font.pixelSize: parent.width*0.05
        visible: ! landscape
    }
}
