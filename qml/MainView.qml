/*****************************************************************************
 * Copyright 2019 Haye Hinrichsen
 * Based on an earlier code by Wolfgang Kinzel and Georg Reents
 *
 * This file is part of Perceptron.
 *
 * Perceptron is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by the
 * Free Software Foundation, either version 3 of the License, or (at your
 * option) any later version.
 *
 * Perceptron is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
 * or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License 
 * for more details.
 *
 * You should have received a copy of the GNU General Public License along 
 * with Perceptron. If not, see http://www.gnu.org/licenses/.
 *****************************************************************************/

import QtQuick 2.12
import QtQuick.Controls 2.4

Item {
    id: mainview
    objectName: "mainview"

    property int numberOfBullets: 15
    readonly property int slots: 10

    property int keyStatus: window.keyStatus
    onKeyStatusChanged:  if (keyStatus != 0) bulletrow.insert(keyStatus,window.prediction)

    Image {
        source: "aluminium.jpg"
        anchors.fill: parent
    }

    Footer {
        id: footer
        anchors.fill: parent
        anchors.topMargin: parent.height-(landscape ? parent.height*0.09 + parent.width*0.06 : parent.height*0.16)
    }

    Item {
        id: greenRegion
        //color: "green"
        anchors.left: parent.left
        anchors.right: parent.right
        anchors.top: parent.top
        anchors.bottom: footer.top
        anchors.rightMargin: parent.width*0.05
        anchors.leftMargin: parent.width*(landscape? 0.1 :0.05)

        Item {
            id: redRegion
            //color: "red"
            anchors.fill: parent
            anchors.rightMargin: widelandscape ? parent.width*0.15 : 0
            anchors.bottomMargin: landscape ? 0 : parent.height*0.3

            property double cellwidth: width / (numberOfBullets)

            BulletRow {
                id: bulletrow
                anchors.top: parent.top
                anchors.topMargin: parent.height*0.03
                anchors.left: parent.left
                anchors.right: parent.right
                height: parent.width*0.11
                cellwidth: parent.cellwidth
            }

            Image {
                id: si
                visible: landscape
                source: "si.png"
                anchors.left: parent.left
                anchors.leftMargin: -window.width*0.06
                anchors.verticalCenter: bulletrow.verticalCenter
                anchors.verticalCenterOffset: bulletrow.height/4
                height: parent.height*0.06
                fillMode: Image.PreserveAspectFit
                width: height
                smooth: true
                mipmap: true
            }

            Weights {
                id: weights
                anchors.top: bulletrow.bottom
                anchors.topMargin: landscape ? parent.height*0.05 : redRegion.height*0.2-redRegion.width*0.1
                anchors.left: parent.left
                anchors.right: parent.right
                height: landscape ? parent.height*0.4 : parent.width*0.35
                cellwidth: parent.cellwidth
            }

            Image {
                visible: landscape
                source: "wi.png"
                anchors.left: si.left
                anchors.verticalCenter: weights.verticalCenter
                height: si.height
                fillMode: Image.PreserveAspectFit
                width: si.width
                smooth: true
                mipmap: true
            }

            Image {
                id: input
                source: "input.png"
                anchors.top: bulletrow.bottom
                anchors.bottom: weights.top
                width: (slots-1)*parent.cellwidth
                x: parent.cellwidth*0.5
                smooth: true
                //mipmap: true
            }

            Image {
                id: collector
                source: "collector.png"
                anchors.top: weights.bottom
                width: (slots-1)*parent.cellwidth
                anchors.bottom: parent.bottom
                anchors.bottomMargin: parent.cellwidth*0.8
                x: parent.cellwidth*0.5
                smooth: true
                //mipmap: true
            }

            Bullet {
                id: prediction
                status: window.prediction
                hidden: ! checkbox.checked
                height: parent.cellwidth*0.7
                width: parent.cellwidth*0.7
                anchors.verticalCenter: collector.bottom
                anchors.horizontalCenter: collector.horizontalCenter

                MyToolTip {
                    anchors.fill: parent
                    text: qsTr("Prediction of your next input")
                }
            }

            Image {
                visible: landscape &&  parent.width > 1.2*parent.height
                source: "result.png"
                anchors.left: si.left
                anchors.bottom: parent.bottom
                anchors.bottomMargin: parent.height*0.02
                height: si.height*3
                fillMode: Image.PreserveAspectFit
                width: si.width*6
                smooth: true
                mipmap: true
            }

            property int vsize: cellwidth*(landscape ? 0.5 : 0.9)

            Item {
                id: showPredictionSwitch
                anchors.verticalCenter: prediction.verticalCenter
                anchors.left: prediction.right
                anchors.leftMargin: parent.cellwidth
                anchors.right: parent.right
                height: parent.vsize

                Text {
                    id: arrow
                    anchors.verticalCenter: parent.verticalCenter
                    anchors.left: parent.left
                    font.pixelSize: redRegion.vsize
                    text: "← "
                }

                MyCheckBox {
                    id: checkbox
                    anchors.verticalCenter: parent.verticalCenter
                    anchors.left: arrow.right
                    height: redRegion.vsize
                    text: qsTr("Show prediction")
                }

                MyToolTip {
                    anchors.fill: parent
                    onPressedChanged: if (pressed) checkbox.checked = !checkbox.checked
                    text: qsTr("Check this box to show/hide the prediction")
                }
            }
        }

        Item {
            id: yellowRegion
            //color: "yellow"
            anchors.left: parent.left
            anchors.leftMargin: landscape ? parent.width*(widelandscape ? 0.85 : 0.75) : 0
            anchors.right: parent.right
            anchors.bottom: parent.bottom
            anchors.top: landscape ? parent.top : redRegion.bottom
            anchors.topMargin: landscape ? (widelandscape ? parent.height*0.05 : parent.height*0.04+parent.width*0.13) : 0
        }

        // Gauge:
        Rectangle {
            anchors.horizontalCenter: yellowRegion.horizontalCenter
            anchors.bottom: yellowRegion.bottom
            anchors.bottomMargin: landscape ? parent.height*0.05 : parent.height*0.05
            width: landscape ? (yellowRegion.height*0.25) : parent.width
            height: landscape ? (yellowRegion.height*0.95) : parent.width*0.2
            radius: landscape ? parent.height*0.02 : width*0.02

            gradient: Gradient {
                GradientStop { position: 0 ; color: "#ddd" }
                GradientStop { position: 1 ; color: "#000" }
            }
            Percentage {
                color: "#444"
                anchors.fill: parent
                anchors.margins: parent.radius/2
                radius: parent.radius
            }
        }
    }

    Separator {
        anchors.horizontalCenter: parent.horizontalCenter
        anchors.verticalCenter: footer.top
    }
}
