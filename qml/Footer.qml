/*****************************************************************************
 * Copyright 2019 Haye Hinrichsen
 * Based on an earlier code by Wolfgang Kinzel and Georg Reents
 *
 * This file is part of Perceptron.
 *
 * Perceptron is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by the
 * Free Software Foundation, either version 3 of the License, or (at your
 * option) any later version.
 *
 * Perceptron is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
 * or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License 
 * for more details.
 *
 * You should have received a copy of the GNU General Public License along 
 * with Perceptron. If not, see http://www.gnu.org/licenses/.
 *****************************************************************************/

import QtQuick 2.12
import QtQuick.Controls 2.4

Rectangle {
    color: "#aaa"

    property int keyStatus: window.keyStatus
    onKeyStatusChanged: {
        if (keyStatus==-1) minus.animate();
        if (keyStatus==1) plus.animate();
    }

    property bool resetStatistics: window.resetStatistics
    onResetStatisticsChanged: {
        if (resetStatistics) clear.animate();
    }

    MyButton {
        id: minus
        height: parent.height*0.6
        anchors.verticalCenter: parent.verticalCenter
        anchors.left: parent.left
        anchors.leftMargin: parent.width*0.15
        width: parent.width*0.2
        color: "green"
        text: "–"
        fontsize: height
        //offset: -fontsize*0.15
        radius: height*0.1
        onPressedChanged: if (pressed) window.input(-1)
        ToolTip {
            parent: minus
            visible: minus.hovered
            delay: 1000
            timeout: 3000
            text: qsTr("Input button")
        }
    }

    MyButton {
        id: toggle
        anchors.verticalCenter: parent.verticalCenter
        anchors.horizontalCenter: parent.horizontalCenter
        anchors.horizontalCenterOffset: (parent.height*0.08+parent.width*0.04)
        width: Math.min(parent.width*0.1,parent.height*0.4)
        height: width
        radius: height*0.1
        text: "RND"
        onColorChanged: if (!pressed) animate()
        color: timer.running ? "orange" : "gray"
        onClicked: window.toggleTimer()
    }

    MyButton {
        id: clear
        text: "CLR"
        anchors.verticalCenter: parent.verticalCenter
        anchors.horizontalCenter: parent.horizontalCenter
        anchors.horizontalCenterOffset: -(parent.height*0.08+parent.width*0.04)
        width: Math.min(parent.width*0.1,parent.height*0.4)
        height: width
        radius: height*0.1
        color: "blue"
        onClicked: {
            window.resetStatistics=false;
            window.resetStatistics=true;
        }
        ToolTip {
            parent: clear
            visible: clear.hovered
            delay: 1000
            timeout: 3000
            text: qsTr("Clear statistics")
        }
    }

    MyButton {
        id:plus
        height: parent.height*0.6
        checkable: true
        anchors.verticalCenter: parent.verticalCenter
        anchors.left: parent.left
        anchors.leftMargin: parent.width*0.65
        width: parent.width*0.2
        color: "red"
        text: "+"
        fontsize: height
        //offset: -fontsize*0.12
        radius: height*0.1
        onPressedChanged: if (pressed) window.input(1)
        ToolTip {
            parent: plus
            visible: plus.hovered
            delay: 1000
            timeout: 3000
            text: qsTr("Input button")
        }
    }
}
