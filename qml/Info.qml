/*****************************************************************************
 * Copyright 2019 Haye Hinrichsen
 * Based on an earlier code by Wolfgang Kinzel and Georg Reents
 *
 * This file is part of Perceptron.
 *
 * Perceptron is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by the
 * Free Software Foundation, either version 3 of the License, or (at your
 * option) any later version.
 *
 * Perceptron is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
 * or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License 
 * for more details.
 *
 * You should have received a copy of the GNU General Public License along 
 * with Perceptron. If not, see http://www.gnu.org/licenses/.
 *****************************************************************************/

import QtQuick 2.12
import QtQuick.Controls 1.4

Item {
    objectName: "info"
    Image {
        source: "aluminium.jpg"
        opacity: 0.6
    }

    Item {
        id: part1
        anchors.fill: parent
        anchors.bottomMargin: landscape ? 0 : parent.height/2
        anchors.rightMargin: landscape ? parent.width/2 : 0

        Image {
            fillMode: Image.PreserveAspectFit
            source: "network.png"
            anchors.top: parent.top
            anchors.topMargin: parent.height*0.05
            height: parent.height*0.25
            anchors.horizontalCenter: parent.horizontalCenter
            smooth: true
            mipmap: true
        }

        Text {
            id: title
            style: Text.Raised;
            styleColor: "white"
            anchors.left: parent.left
            anchors.right: parent.right
            anchors.top: parent.top
            anchors.topMargin: parent.height*0.33
            text: "Perceptron 1.0"
            horizontalAlignment: Text.AlignHCenter
            font.family: window.myfont
            font.pixelSize: parent.height*0.04+parent.width*0.03
            //font.capitalization: Font.SmallCaps
        }

        Text {
            wrapMode: Text.WordWrap
            id: names
            lineHeight: 0.7
//            style: Text.Raised;
//            styleColor: "white"
            anchors.left: parent.left
            anchors.right: parent.right
            anchors.top: title.bottom
            anchors.topMargin: parent.height*0.05
            text: "Wolfgang Kinzel\nGeorg Reents\nHaye Hinrichsen\n\n" + "TP3, " + qsTr("University of Würzburg, Germany")
            horizontalAlignment: Text.AlignHCenter
            font.family: window.myfont
            font.pixelSize: parent.height*0.02+parent.width*0.02
            //font.capitalization: Font.SmallCaps
        }

        Text {
            wrapMode: Text.WordWrap
            anchors.left: parent.left
            anchors.right: parent.right
            anchors.top: names.bottom
            anchors.topMargin: parent.height*0.05
            textFormat: Text.RichText
            onLinkActivated: Qt.openUrlExternally(link);
            //linkColor: "#000"
            text: qsTr("License:") +
                  " <a href=\"https://www.gnu.org/licenses/gpl-3.0.en.html\" style=\"color: rgb(0,64,64)\">GNU GPLv3</a>, " +
                  qsTr("based on") +
                  " <a href=\"https://www.qt.io\" style=\"color: rgb(0,64,64)\">Qt</a><br>" +
                  qsTr("Source code repository:") + " <a href=\"https://gitlab.com/tp3/perceptron\" style=\"color: rgb(0,64,64)\">gitlab.com/tp3</a>"
            horizontalAlignment: Text.AlignHCenter
            font.family: window.myfont
            font.pixelSize: parent.height*0.02+parent.width*0.015
        }
    }

    Item {
        id: part2
        anchors.fill: parent
        anchors.topMargin: landscape ? 0 : parent.height/2
        anchors.leftMargin: landscape ? parent.width/2 : 0

        Item {
            id: languageselector
            anchors.top: parent.top
            anchors.topMargin: parent.height * 0.1
            anchors.horizontalCenter: parent.horizontalCenter
            width: parent.width*0.7
            height: width*0.07

            Text {
                id: language
                anchors.fill: parent
                font.family: myfont
                verticalAlignment: Text.AlignVCenter
                font.pixelSize: height*0.8
                text: qsTr("Language:")
            }
            MyComboBox {
                anchors.fill: parent
                anchors.leftMargin: language.contentWidth+parent.width*0.05
                model: translator.languages
                currentIndex: translator.index
                fontsize: parent.height*0.8
                onCurrentIndexChanged: if (count>0) translator.index=currentIndex
            }
        }

        Column {
            id: textarea
            property int fontsize: (parent.width+parent.height)*0.018
            anchors.left: parent.left
            anchors.leftMargin: parent.width*0.05
            anchors.right: parent.right
            anchors.rightMargin: parent.width*0.02
            anchors.top: languageselector.bottom
            anchors.topMargin: parent.height*0.05
            anchors.bottom: parent.bottom
            Text {
                //anchors.fill: parent
                width: parent.width
                font.family: window.myfont
                font.pixelSize: parent.fontsize
                lineHeight: 0.7
//                style: Text.Raised;
//                styleColor: "white"
                wrapMode: Text.WordWrap
                text: qsTr("The perceptron is the basic element of a neural network.")+" "+
                      qsTr("This application shows how the perceptron learns and predicts a binary sequence.")+"\n\n● "+
                      qsTr("First, press the red and the green button in a repeating (e.g. alternating) pattern:") + "\n"
            }

            Image {
                source: "alternating.png"
                width: parent.width/2
                anchors.horizontalCenter: parent.horizontalCenter
                fillMode: Image.PreserveAspectFit
            }

            Text {
                width: parent.width
                font.family: window.myfont
                font.pixelSize: parent.fontsize
                lineHeight: 0.7
//                style: Text.Raised;
//                styleColor: "white"
                wrapMode: Text.WordWrap
                text: "\n"+
                      qsTr("As you can see, the synaptic weights quickly adjust themselves, and the perceptron predicts the sequence correctly after short time.")+"\n\n● "+
                      qsTr("Next, press the buttons randomly.") +" "+
                      qsTr("If your input is perfectly random, only 50% of the predictions are expected to be correct.")+" "+
                      qsTr("If the gauge shows more than 50%, this indicates hidden correlations, meaning that you are not a perfect random number generator.")
            }
        }
    }

    Separator {
        anchors.centerIn: parent
        horizontal: ! landscape
    }
}
