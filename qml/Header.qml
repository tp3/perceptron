/*****************************************************************************
 * Copyright 2019 Haye Hinrichsen
 * Based on an earlier code by Wolfgang Kinzel and Georg Reents
 *
 * This file is part of Perceptron.
 *
 * Perceptron is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by the
 * Free Software Foundation, either version 3 of the License, or (at your
 * option) any later version.
 *
 * Perceptron is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
 * or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License 
 * for more details.
 *
 * You should have received a copy of the GNU General Public License along 
 * with Perceptron. If not, see http://www.gnu.org/licenses/.
 *****************************************************************************/

import QtQuick 2.0

Rectangle {
    id: header
    color: "#aaa"

    MyToolButton {
        source: "exit.png"
        tooltip: qsTr("Exit");
        anchors.verticalCenter: parent.verticalCenter
        anchors.right: parent.right
        height: parent.height*0.6
        width: height
        opacity: 0.6
        onPressedChanged: if (!pressed) if (stack.currentItem.objectName=="info") stack.pop(); else Qt.quit()
    }

    MyToolButton {
        source: "info.png"
        tooltip: qsTr("About this application")
        anchors.verticalCenter: parent.verticalCenter
        anchors.left: parent.left
        height: parent.height*0.85
        width: height
        onPressedChanged: if (!pressed) if (stack.currentItem.objectName=="info") stack.pop(); else stack.push(info)
    }

    Text {
        style: Text.Raised;
        styleColor: "white"
        anchors.fill: parent
        text: qsTr("Perceptron")
        verticalAlignment: Text.AlignVCenter
        horizontalAlignment: Text.AlignHCenter
        font.family: window.myfont
        font.pixelSize: height*0.4
    }
}
