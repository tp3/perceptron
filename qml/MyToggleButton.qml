/*****************************************************************************
 * Copyright 2019 Haye Hinrichsen
 * Based on an earlier code by Wolfgang Kinzel and Georg Reents
 *
 * This file is part of Perceptron.
 *
 * Perceptron is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by the
 * Free Software Foundation, either version 3 of the License, or (at your
 * option) any later version.
 *
 * Perceptron is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
 * or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License 
 * for more details.
 *
 * You should have received a copy of the GNU General Public License along 
 * with Perceptron. If not, see http://www.gnu.org/licenses/.
 *****************************************************************************/

import QtQuick 2.12
import QtQuick.Controls 2.4

Item {
    id: mybutton
    property bool pressed: false
    property int fontsize: height*0.47
    property int radius: 0
    property color color: "black"
    width: 200
    height: 50

    Rectangle {
        anchors.fill: parent
        radius: mybutton.radius*1.1
        anchors.margins: -parent.height*0.05
        gradient: Gradient {
            GradientStop { position: 0 ; color: "#fff" }
            GradientStop { position: 1 ; color: "#000" }
        }

    }
    Rectangle {
        anchors.fill: parent
        border.width: 1
        border.color: "#888"
        radius: mybutton.radius
        gradient: Gradient {
            GradientStop { position: 0 ; color: mybutton.pressed ? "white" : "white" }
            GradientStop { position: 1 ; color: mybutton.pressed ? "orange" : "gray" }
        }
    }

    Text {
        anchors.fill: parent
        text: "RND"
        font.bold: pressed
        verticalAlignment: Text.AlignVCenter
        horizontalAlignment: Text.AlignHCenter
        font.family: window.myfont
        font.pixelSize: fontsize
    }

    MyToolTip {
        anchors.fill: parent
        onPressedChanged: if(pressed) mybutton.pressed = ! mybutton.pressed
        text: qsTr("Emulate random input")
    }
}


