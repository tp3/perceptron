/*****************************************************************************
 * Copyright 2019 Haye Hinrichsen
 * Based on an earlier code by Wolfgang Kinzel and Georg Reents
 *
 * This file is part of Perceptron.
 *
 * Perceptron is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by the
 * Free Software Foundation, either version 3 of the License, or (at your
 * option) any later version.
 *
 * Perceptron is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
 * or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License 
 * for more details.
 *
 * You should have received a copy of the GNU General Public License along 
 * with Perceptron. If not, see http://www.gnu.org/licenses/.
 *****************************************************************************/

//=============================================================================
//                                Perceptron
//                              Main QML file
//=============================================================================

import QtQuick 2.12
import QtQuick.Controls 2.5
import Qt.labs.settings 1.0
import Perceptron 1.0

ApplicationWindow {
    id: window
    visible: true
    width: 640
    height: 480
    title: qsTr("Physics Apps - Perceptron")

    property bool landscape: width>0.8*height;
    property bool widelandscape: width>1.5*height;
    readonly property string myfont: "Times"
    property int keyStatus: 0
    property int prediction: 0
    property int correctGuess: 0
    property bool resetStatistics: false

    //----------------------------- Settings ----------------------------------

    Settings {
        id: settings
        property alias x: window.x
        property alias y: window.y
        property alias width: window.width
        property alias height: window.height
    }

    //----------------------------- Translator ---------------------------------

    Translator {
        id: translator
    }

    //-------------------------- Signals and Slots -----------------------------

    function getPrediction (value) {
        prediction = value
    }

    property int lastSlot: -1
    property double lastWeight: 0
    function getWeight (index,weight) { if (index>=0) {lastWeight = weight; lastSlot = index;}  }

    signal sendInput (int value);

    function input (value) {
        keyStatus = value; keyStatus = 0;
        correctGuess=0; correctGuess=value*prediction;
        sendInput(value)
    }

    Header {
        id: header
        anchors.fill: parent
        anchors.bottomMargin: parent.height-parent.width*(landscape ? 0.08 : 0.15)
    }

    Separator {
        anchors.horizontalCenter: parent.horizontalCenter
        anchors.verticalCenter: header.bottom
    }

    StackView {
        id: stack
        anchors.top: header.bottom
        anchors.left: parent.left
        anchors.right: parent.right
        anchors.bottom: parent.bottom
        initialItem: introduction

        Component {
            id: introduction
            Introduction {
            }
        }

        Component {
            id: mainview
            MainView {
                id: mainviewItem
            }
        }

        Component {
            id: info
            Info {
            }
        }


        focus: true
        Keys.onPressed: {
            var name = stack.currentItem.objectName
            if (name=="introduction" && event.key !== Qt.Key_Escape) push(mainview);
            else switch(event.key) {
            case Qt.Key_F4:
            case Qt.Key_E:
            case Qt.Key_Q:              Qt.quit(); break;
            case Qt.Key_Escape:         if (name=="mainview") {
                                            window.resetStatistics=false;
                                            window.resetStatistics=true;
                                        }
                                        else if (name=="introduction") Qt.quit();
                                        else stack.pop();
                                        break;
            case Qt.Key_Left:
            case Qt.Key_Comma:
            case Qt.Key_Minus:
            case Qt.Key_0:              input(-1); break;
            case Qt.Key_Period:
            case Qt.Key_Right:
            case Qt.Key_Plus:
            case Qt.Key_1:              input(1); break;
            case Qt.Key_F1:
            case Qt.Key_H:
            case Qt.Key_I:              if (name=="mainview") stack.push(info);
                                        else if (name=="info") stack.pop(); break;
            case Qt.Key_C:              window.resetStatistics=false;
                                        window.resetStatistics=true;
                                        break;
            case Qt.Key_R:              toggleTimer();
            }
        }
    }

    Timer {
        id: timer
        interval: 1000
        running: false
        repeat: true
        onTriggered: input(Math.random() >0.5 ? 1 : -1);
    }

    function toggleTimer() {
        if (timer.running) timer.stop();
        else timer.start();
    }
}
