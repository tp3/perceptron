/*****************************************************************************
 * Copyright 2019 Haye Hinrichsen
 * Based on an earlier code by Wolfgang Kinzel and Georg Reents
 *
 * This file is part of Perceptron.
 *
 * Perceptron is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by the
 * Free Software Foundation, either version 3 of the License, or (at your
 * option) any later version.
 *
 * Perceptron is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
 * or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License 
 * for more details.
 *
 * You should have received a copy of the GNU General Public License along 
 * with Perceptron. If not, see http://www.gnu.org/licenses/.
 *****************************************************************************/

import QtQuick 2.0
import QtQuick.Controls 2.4

Item {
    id: bulletrow
    property int cellwidth: 10

    property double vsize: width*0.05
    property int shift: 0
    property double displacement: 0
    property int cursor: 0
    property double threshold: numberOfBullets * width  / (numberOfBullets)
    property bool resetStatistics: window.resetStatistics

    onResetStatisticsChanged: {
        for (var i=0; i<numberOfBullets; ++i) {
            bullets.itemAt(i).status=0;
            yesno.itemAt(i).status=0;
        }
        shift=0
        displacement=0;
        cursor=0;
    }

    function insert (choice,prediction) {
        displacement=shift;
        shift++;
        if (cursor==0) cursor=numberOfBullets
        cursor--;

        bullets.itemAt(cursor).status=choice
        yesno.itemAt(cursor).status=(choice===prediction ? 1:-1)
        animation.restart();
    }

    MyToolTip {
        id: upper
        anchors.fill: parent
        anchors.bottomMargin: parent.height/2
        text: qsTr("Correct and incorrect predictions")
    }

    MyToolTip {
        id: lower
        anchors.fill: parent
        anchors.topMargin: parent.height/2
        text: qsTr("Input data")
    }

    Repeater {
        id: bullets
        model: 15
        Bullet {
            anchors.bottom: bulletrow.bottom
            status: 0
            height: vsize
            width: cellwidth
            property double xc: ((index+displacement+1)*parent.width/numberOfBullets)%parent.width
            x: xc-cellwidth
            opacity: xc < cellwidth ? (xc/cellwidth) : xc > parent.width-cellwidth ? (parent.width-xc)/cellwidth : 1
        }
    }
    Repeater {
        id: yesno
        model: numberOfBullets
        YesNoSign {
            anchors.top: bulletrow.top
            status: 0
            height: vsize
            width: cellwidth
            property double xc: ((index+displacement+1)*parent.width/numberOfBullets)%parent.width
            x: xc-cellwidth
            opacity: xc < cellwidth ? (xc/cellwidth) : xc > parent.width-cellwidth ? (parent.width-xc)/cellwidth : 1
        }
    }

    PropertyAnimation {
        id: animation
        target: bulletrow
        property: "displacement"
        to: shift
        duration: 100
    }
}
