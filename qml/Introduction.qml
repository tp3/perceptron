/*****************************************************************************
 * Copyright 2019 Haye Hinrichsen
 * Based on an earlier code by Wolfgang Kinzel and Georg Reents
 *
 * This file is part of Perceptron.
 *
 * Perceptron is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by the
 * Free Software Foundation, either version 3 of the License, or (at your
 * option) any later version.
 *
 * Perceptron is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
 * or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License 
 * for more details.
 *
 * You should have received a copy of the GNU General Public License along 
 * with Perceptron. If not, see http://www.gnu.org/licenses/.
 *****************************************************************************/

import QtQuick 2.12

Item {
    objectName: "introduction"


    Image {
        source: "neurons.jpg"
        anchors.fill: parent

    }

    Rectangle {
        anchors.centerIn: parent
        width: (parent.height+parent.width)*0.15
        height: width
        radius: width*0.2
        gradient: Gradient {
            GradientStop {
                position: 0.00;
                color: "#eee";
            }
            GradientStop {
                position: 1.00;
                color: "#222";
            }
        }
        Image {
            anchors.fill: parent
            source: "logo-enlarged.png"
            smooth: true
            mipmap: true
        }
    }

    MouseArea {
        anchors.fill: parent
        onClicked: if (mouseY > height*0.9)
            Qt.openUrlExternally("https://commons.wikimedia.org/wiki/File:CPCA-GFAP,MCA-5B10,Tau,neurons.jpg")
        else if (stack.currentItem.objectName==="introduction") stack.push(mainview);
    }

    Text {
        color: "#eee"
        anchors.fill: parent
        anchors.margins: (parent.height+parent.width)*0.005
        text: "Picture by Gary Shaw [Wikimedia]"
        horizontalAlignment: Text.AlignRight
        verticalAlignment: Text.AlignBottom
        height: (parent.width)*0.02;
        font.family: myfont
    }

    Timer {
        id: splash
        repeat: false
        interval: 2500
        onTriggered: if (stack.currentItem.objectName==="introduction") stack.push(mainview)
    }

    Component.onCompleted: splash.start()
}
