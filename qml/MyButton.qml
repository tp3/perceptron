/*****************************************************************************
 * Copyright 2019 Haye Hinrichsen
 * Based on an earlier code by Wolfgang Kinzel and Georg Reents
 *
 * This file is part of Perceptron.
 *
 * Perceptron is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by the
 * Free Software Foundation, either version 3 of the License, or (at your
 * option) any later version.
 *
 * Perceptron is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
 * or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License 
 * for more details.
 *
 * You should have received a copy of the GNU General Public License along 
 * with Perceptron. If not, see http://www.gnu.org/licenses/.
 *****************************************************************************/

import QtQuick 2.12
import QtQuick.Controls 1.3
import QtQuick.Controls.Styles 1.4

Button {
    id: mybutton
    property int fontsize: height*0.47
    property int offset: 0
    property int radius: 0
    property color color: "black"
    function animate() { animation.restart() }
    width: 200
    height: 50
    text: "undefined"
    scale: pressed ? 1.05 : 1
    style: ButtonStyle {
        label: Item {
            anchors.fill: parent
            Text {
                anchors.centerIn: parent
                anchors.verticalCenterOffset: mybutton.offset
                renderType: Text.NativeRendering
                horizontalAlignment: Text.AlignHCenter
                verticalAlignment: Text.AlignVCenter
                font.family: window.myfont
                font.pixelSize: fontsize
                color: "black"
                text: control.text
            }
        }
        background: Item {
            Rectangle {
                anchors.fill: parent
                radius: mybutton.radius*1.1
                anchors.margins: -parent.height*0.05
                gradient: Gradient {
                    GradientStop { position: 0 ; color: "#fff" }
                    GradientStop { position: 1 ; color: "#000" }
                }

            }
            Rectangle {
                anchors.fill: parent
                border.width: control.activeFocus ? 2 : 1
                border.color: "#888"
                radius: mybutton.radius
                gradient: Gradient {
                    GradientStop { position: 0 ; color: control.pressed ? mybutton.color : "#ddd" }
                    GradientStop { position: 1 ; color: mybutton.color }
                }
            }
        }
    }
    PropertyAnimation {
        id: animation
        duration: 100
        target: mybutton
        property: "scale"
        from: 1.06
        to:  pressed ? 1.05 : 1
    }
}


