/*****************************************************************************
 * Copyright 2019 Haye Hinrichsen
 * Based on an earlier code by Wolfgang Kinzel and Georg Reents
 *
 * This file is part of Perceptron.
 *
 * Perceptron is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by the
 * Free Software Foundation, either version 3 of the License, or (at your
 * option) any later version.
 *
 * Perceptron is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
 * or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License 
 * for more details.
 *
 * You should have received a copy of the GNU General Public License along 
 * with Perceptron. If not, see http://www.gnu.org/licenses/.
 *****************************************************************************/

import QtQuick 2.12

Item {
    property alias source: img.source
    property alias pressed: mouse.pressed
    property alias tooltip: mouse.text

    MyToolTip {
        id: mouse
        anchors.fill: parent
    }

    Image {
        id: img
        anchors.fill: parent
        scale: mouse.containsMouse ? (mouse.pressed ? 1.2 : 1.1) : 1
        smooth: true
        mipmap: true
    }

}
