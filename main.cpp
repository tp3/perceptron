/*****************************************************************************
 * Copyright 2019 Haye Hinrichsen
 * Based on an earlier code by Wolfgang Kinzel and Georg Reents
 *
 * This file is part of Perceptron.
 *
 * Perceptron is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by the
 * Free Software Foundation, either version 3 of the License, or (at your
 * option) any later version.
 *
 * Perceptron is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
 * or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License 
 * for more details.
 *
 * You should have received a copy of the GNU General Public License along 
 * with Perceptron. If not, see http://www.gnu.org/licenses/.
 *****************************************************************************/

#include <QGuiApplication>
#include <QQmlApplicationEngine>

#include "application.h"
#include "translator.h"

int main(int argc, char *argv[])
{
    QCoreApplication::setAttribute(Qt::AA_EnableHighDpiScaling);
    QGuiApplication app(argc, argv);

    // App identity
    app.setOrganizationName("Perceptron");
    app.setOrganizationDomain("University of Wuerzburg");
    app.setApplicationName("perceptron.tp3.app");

    // Register the translator
    qmlRegisterType<Translator> ("Perceptron",1,0,"Translator");

    // Load the graphical user interface engine
    QQmlApplicationEngine engine;
    engine.load(QUrl(QStringLiteral("qrc:/main.qml")));
    if (engine.rootObjects().isEmpty()) return -1;

    // Instanciate and initialize the application
    Application application;
    application.init(engine);

    // Initialize translator
    Translator::init(app,engine,"perceptron",":/languages/translations");

    // Execute the application
    return app.exec();
}
