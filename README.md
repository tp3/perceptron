This app demonstrates how a simple perceptron works. The user types a sequence
of bits and the perceptron tries to predict the next input. The app displays 
the synaptic weights, the actual prediction, and the percentage of correctly 
predicted hits.
