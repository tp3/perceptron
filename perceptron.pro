QT += quick
CONFIG += c++11
TARGET = perceptron

# The following define makes your compiler emit warnings if you use
# any Qt feature that has been marked deprecated (the exact warnings
# depend on your compiler). Refer to the documentation for the
# deprecated API to know how to port your code away from it.
DEFINES += QT_DEPRECATED_WARNINGS

# You can also make your code fail to compile if it uses deprecated APIs.
# In order to do so, uncomment the following line.
# You can also select to disable deprecated APIs only up to a certain version of Qt.
#DEFINES += QT_DISABLE_DEPRECATED_BEFORE=0x060000    # disables all the APIs deprecated before Qt 6.0.0

SOURCES += \
    main.cpp \
    application.cpp \
    translator.cpp

HEADERS += \
    application.h \
    translator.h \
    shared.h

RESOURCES += \
    qml/qml.qrc \
    media/media.qrc \
    translations/languages.qrc \

# Additional import path used to resolve QML modules in Qt Creator's code model
QML_IMPORT_PATH =

# Additional import path used to resolve QML modules just for Qt Quick Designer
QML_DESIGNER_IMPORT_PATH =

# Default rules for deployment.
qnx: target.path = /tmp/$${TARGET}/bin
else: unix:!android: target.path = /opt/$${TARGET}/bin
!isEmpty(target.path): INSTALLS += target

#=============================== Translations =================================

TRANSLATIONS = \
    translations/$${TARGET}_en.ts \ # default language
    translations/$${TARGET}_de.ts \
    translations/$${TARGET}_fr.ts \
    translations/$${TARGET}_es.ts \
    translations/$${TARGET}_it.ts \
    translations/$${TARGET}_ru.ts \
    translations/$${TARGET}_zh.ts \
    translations/$${TARGET}_ja.ts \
    translations/$${TARGET}_ko.ts \


# Include QML files as sources:
lupdate_only { SOURCES += qml/*.qml }

DISTFILES += \
    android/AndroidManifest.xml \
    android/gradle/wrapper/gradle-wrapper.jar \
    android/gradlew \
    android/res/values/libs.xml \
    android/build.gradle \
    android/gradle/wrapper/gradle-wrapper.properties \
    android/gradlew.bat

contains(ANDROID_TARGET_ARCH,armeabi-v7a) {
    ANDROID_PACKAGE_SOURCE_DIR = \
        $$PWD/android
}

