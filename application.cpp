/*****************************************************************************
 * Copyright 2019 Haye Hinrichsen
 * Based on an earlier code by Wolfgang Kinzel and Georg Reents
 *
 * This file is part of Perceptron.
 *
 * Perceptron is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by the
 * Free Software Foundation, either version 3 of the License, or (at your
 * option) any later version.
 *
 * Perceptron is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
 * or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License 
 * for more details.
 *
 * You should have received a copy of the GNU General Public License along 
 * with Perceptron. If not, see http://www.gnu.org/licenses/.
 *****************************************************************************/

#include <QQuickWindow>
#include <cmath>

#include "application.h"

//--------------------------------- Constructor -------------------------------

Application::Application(): N(10), S(N,0), J(N,0), h(0)
{}

//------------------------------- Initialization -------------------------------

void Application::init(QQmlApplicationEngine &engine)
{
    // Determine the main QML window
    QObject *mainwindow = engine.rootObjects().value(0);
    QQuickWindow *qml = qobject_cast<QQuickWindow *>(mainwindow);

    // Connect the graphical user interface written in QML
    connect (this,SIGNAL(sendPrediction(QVariant)),qml,SLOT(getPrediction(QVariant)));
    connect (this,SIGNAL(sendWeight(QVariant,QVariant)),qml,SLOT(getWeight(QVariant,QVariant)));
    connect (qml,SIGNAL(sendInput(int)),this,SLOT(getInput(int)));
}

//--------------------- Public slot: get input -1/+1 ----------------------------

void Application::getInput(int input)
{
    learn(input);
    for (int i=N-1; i>0; --i) S[i]=S[i-1];    // Shift input neurons to the right
    S[0]=input;
    predict();
}

//--------------------------- Predict the next hit -------------------------------

void Application::predict()
{
    h=0;
    for (int i=0; i<N; i++) h+=J[i]*S[i];           // compute weighted sum
    emit sendPrediction (h>=0 ? 1:-1);
}


//------------------------- Perceptron leanring rule -----------------------------

void Application::learn (int input)
{
    double squaredSum=0;
    for (auto j:J) squaredSum += j*j;
    if (h*input <= sqrt(squaredSum)) for (int i=0; i<N; ++i)
    {
         J[i] += static_cast<double>(input*S[i])/N;
         emit sendWeight(i,J[i]);
    }
}
