/*****************************************************************************
 * Copyright 2019 Haye Hinrichsen
 * Based on an earlier code by Wolfgang Kinzel and Georg Reents
 *
 * This file is part of Perceptron.
 *
 * Perceptron is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by the
 * Free Software Foundation, either version 3 of the License, or (at your
 * option) any later version.
 *
 * Perceptron is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
 * or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License 
 * for more details.
 *
 * You should have received a copy of the GNU General Public License along 
 * with Perceptron. If not, see http://www.gnu.org/licenses/.
 *****************************************************************************/


#ifndef SHARED_H
#define SHARED_H

// additional defines for windows msvc
// support for or, and, not operators
#if defined(_WIN32) || defined(_WIN64)
#include <iso646.h>
using uint = unsigned int;
#endif


#endif // SHARED_H
